<?php
// Load required classes
if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/controller-content-feed.php');

class ContentFeedTable extends WP_List_Table {

    function get_columns() {
        return array(
            'nid' => 'ID',
            'title' => 'Title',
            'authors' => 'Authors',
            'created' => 'Date',
            'status' => 'Status',
            'actions' => ''
        );
    }

    function get_sortable_columns() {
        return array(
            'nid' => array('nid', false),
            'title' => array('title', false),
            'authors' => array('authors', false),
            'created' => array('created', false),
            'status' => array('status', false)
        );
    }

    function prepare_items() {
        global $wpdb;

        // columns
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        // pagination control
        $recordsPerPage = 50;
        $currentPage = $this->get_pagenum();
        $offset = 0;
        if ($currentPage > 1) {
            $offset = $recordsPerPage * ($currentPage - 1);
        }

        // search
        $searchSQL = '';
        $s = (isset($_REQUEST['s']) && !empty($_REQUEST['s'])) ? trim($_REQUEST['s']) : '';
        if (!empty($s)) {
            $searchSQL .= " AND (title LIKE '%" . esc_sql($wpdb->esc_like($s)) . "%' OR authors LIKE '%" . esc_sql($wpdb->esc_like($s)) . "%' OR nid = " . ((int) $s) . ") ";
        }

        // hides by default any Unpublished item
        $searchSQL .= " AND (status = '1') ";

        // order
        $orderBy = (isset($_REQUEST['orderby']) && !empty($_REQUEST['orderby'])) ? trim($_REQUEST['orderby']) : 'created';
        $order = (isset($_REQUEST['order']) && !empty($_REQUEST['order'])) ? trim($_REQUEST['order']) : 'desc';
        if (!in_array($orderBy, array('nid', 'title', 'authors', 'created', 'status'))) {
            $orderBy = 'created';
        }
        if (!in_array($order, array('asc', 'desc'))) {
            $order = 'desc';
        }

        // Get list of existing articles in the database
        $tablePosts = $wpdb->prefix . 'posts';
        $tablePostMeta = $wpdb->prefix . 'postmeta';
        $posts = $wpdb->get_results("SELECT pm.meta_value FROM {$tablePosts} p INNER JOIN {$tablePostMeta} pm ON p.ID = pm.post_id AND pm.meta_key = 'fandl_nid' WHERE p.post_type = 'fandl_feed_article' AND p.post_status IN ('publish', 'draft', 'future', 'private')", OBJECT_K);
        $posts = array_keys($posts);

        // controller
        $controller = new ContentFeedController();
        $table = $controller->getFeedTableName();

        // items
        $items = $wpdb->get_results("SELECT * FROM {$table} WHERE 1 = 1 {$searchSQL}" . $wpdb->prepare("ORDER BY {$orderBy} {$order} LIMIT %d OFFSET %d", $recordsPerPage, $offset), ARRAY_A);
        $totalItems = $wpdb->get_var("SELECT COUNT(id) FROM {$table} WHERE 1 = 1 {$searchSQL}");

        // Add actions for each item
        $basePluginUrl = plugin_dir_url(__DIR__);
        foreach ($items as $itemKey => $item) {
            if (isset($item['status']) && ($item['status'] == '1')) {
                if (in_array($item['nid'], $posts)) {
                    $item['actions'] = '<a href="' . $basePluginUrl . 'import-node.php?nid=' . $item['nid'] . '" class="button action">Update</a>';
                } else {
                    $item['actions'] = '<a href="' . $basePluginUrl . 'import-node.php?nid=' . $item['nid'] . '" class="button-primary">Import</a>';
                }
            }
            $items[$itemKey] = $item;
        }

        $this->items = $items;

        // pagination
        $this->set_pagination_args(array(
            'total_items' => $totalItems,
            'per_page' => $recordsPerPage,
            'total_pages' => ceil($totalItems / $recordsPerPage)
        ));
    }

    function column_default($item, $columnName) {
        switch ($columnName) {
            case 'nid':
            case 'title':
            case 'authors':
            case 'created':
            case 'actions':
                return $item[$columnName];
            case 'status':
                return (($item[$columnName] == '1') ? 'Published' : 'Unpublished');
            default:
                return print_r($item, true);
        }
    }

}
