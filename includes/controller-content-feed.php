<?php
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');

// Load required classes
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/class-content-feed-api.php');

class ContentFeedController {

    public function truncateFeed() {
        global $wpdb;

        // Table name
        $table = $this->getFeedTableName();

        // Truncate table
        return $wpdb->query('TRUNCATE TABLE ' . $table);
    }

    public function importFeed() {
        global $wpdb;

        // Content Feed API
        $contentFeedApi = new ContentFeedApi();
        $data = $contentFeedApi->requestApiList();
        if ($data) {
            // Table name
            $table = $this->getFeedTableName();

            // List of all existing feed records
            $feedArray = array();
            $feedResults = $wpdb->get_results("SELECT nid, changed, status FROM {$table}", ARRAY_A);
            foreach ($feedResults as $feedRecord) {
                $feedRecordNid = (int) $feedRecord['nid'];

                // Add to array
                $feedArray[$feedRecordNid] = array(
                    'changed' => strtotime($feedRecord['changed']),
                    'status' => (bool) $feedRecord['status']
                );
            }

            // Process all records from the API request
            foreach ($data as $content) {
                if (empty($content['id']) || empty($content['title']) || empty($content['created'])) {
                    continue;
                }

                // Relevant fields from the API feed record
                $nid = (int) $content['id'];
                $changed = $content['changed'];
                if ($changed) {
                    $changed = strtotime($changed);
                }
                $status = (bool) $content['status'];

                // Prepare record for database
                $record = [
                    'nid' => $nid,
                    'title' => $content['title'],
                    'created' => date('Y-m-d H:i:s', strtotime($content['created'])),
                    'status' => ((isset($content['status']) && !empty($content['status'])) ? trim($content['status']) : null)
                ];
                if ($content['authors']) {
                    $record['authors'] = $content['authors'];
                }
                if ($content['changed']) {
                    $record['changed'] = date('Y-m-d H:i:s', strtotime($content['changed']));
                }

                // Check if API record exists in the feed table
                if (isset($feedArray[$nid])) {
                    // Check if feed record needs to be updated (more recent update in F&L or status is different)
                    if (($changed > $feedArray[$nid]['changed']) || ($status != $feedArray[$nid]['status'])) {
                        // Update
                        $wpdb->update($table, $record, array('nid' => $nid));
                    }
                } else {
                    // Insert
                    $wpdb->insert($table, $record);
                }
            } // foreach

            return true;
        }
        return false;
    }

    public function importNode($nid) {
        global $wpdb;

        // Read in existing option value from database
        $apiBaseUrl = get_option('fandl_feed_api_base_url');
        if (substr($apiBaseUrl, -1) == '/') {
            $apiBaseUrl = substr($apiBaseUrl, 0, -1);
        }

        // Content Feed API
        $contentFeedApi = new ContentFeedApi();
        $data = $contentFeedApi->requestApiNode($nid);
        if ($data) {
            $data = reset($data);
            $postData = array();

            // Check if content already exists
            $query = new WP_Query(array(
                'post_type' => 'fandl_feed_article',
                'posts_per_page' => 1,
                'post_status' => 'any',
                'meta_query' => array(
                    array(
                        'key' => 'fandl_nid',
                        'compare' => '=',
                        'value' => $nid
                    )
                )
            ));
            if ($query->have_posts()) {
                $query->the_post();

                // Set the ID so the post gets updated
                $postData['ID'] = get_the_ID();

                // Load all existing post fields
                $postData = get_post($postData['ID'], ARRAY_A, 'db');
            }
            wp_reset_postdata();

            // Preprocess fields
            $body = preg_replace("/<img([^>]*) src=\"[^http|ftp|https]([^\"]*)\"/i", "<img\${1} src=\"" . $apiBaseUrl . "\/\${2}\"", $data['body']);
            $abstract = preg_replace("/<img([^>]*) src=\"[^http|ftp|https]([^\"]*)\"/i", "<img\${1} src=\"" . $apiBaseUrl . "\/\${2}\"", $data['abstract']);
            $imageCaption = preg_replace("/<img([^>]*) src=\"[^http|ftp|https]([^\"]*)\"/i", "<img\${1} src=\"" . $apiBaseUrl . "\/\${2}\"", $data['image']['caption']);

            // Remove duplicate line breaks to avoid issues with WordPress auto paragraph feature
            $body = preg_replace("/([\r\n]{4,}|[\n]{2,}|[\r]{2,})/i", "\n", $body);
            $abstract = preg_replace("/([\r\n]{4,}|[\n]{2,}|[\r]{2,})/i", "\n", $abstract);

            // Remove multiple whitespaces
            $body = preg_replace('/[ ]{2,}/i', '', $body);
            $abstract = preg_replace('/[ ]{2,}/i', '', $abstract);

            // Post fields
            $postData['post_title'] = $data['title'];
            $postData['post_content'] = $body;
            $postData['post_excerpt'] = $abstract;
            $postData['post_status'] = 'publish';
            $postData['post_date'] = $data['article_date'];
            $postData['post_type'] = 'fandl_feed_article';
            $postData['meta_input'] = array(
                'fandl_nid' => (int) $data['id'],
                'fandl_image_caption' => $imageCaption,
                'fandl_image_credit' => $data['image']['credit'],
                'fandl_canonical_url' => $data['metatags']['canonical_url'],
                'fandl_metatag_title' => $data['metatags']['title'],
                'fandl_metatag_description' => $data['metatags']['description'],
                'fandl_article_date' => $data['article_date'],
                'fandl_changed' => $data['changed'],
                'fandl_created' => $data['created']
            );

            // Insert or Update post
            if ($postId = wp_insert_post($postData)) {
                // Image
                if (!empty($data['image']['url'])) {
                    // Check if image already exists
                    $queryImage = new WP_Query(array(
                        'post_type' => 'attachment',
                        'posts_per_page' => 1,
                        'post_status' => 'any',
                        'title' => $data['image']['name']
                    ));
                    if ($queryImage->have_posts()) {
                        $queryImage->the_post();

                        // Set as featured image
                        set_post_thumbnail($postId, get_the_ID());
                    } else {
                        $imageUrl = str_replace($apiBaseUrl, 'https://www.faithandleadership.com', $data['image']['url']);

                        // Will return the attachment id of the sideloaded image
                        $image = media_sideload_image($imageUrl, $postId, $data['image']['name'], 'id');

                        // Set as featured image
                        set_post_thumbnail($postId, $image);
                    }
                    wp_reset_postdata();
                }

                // Topics
                if (!empty($data['topics'])) {
                    wp_set_object_terms($postId, $data['topics'], 'fandl_feed_topics', true);
                }

                // Authors
                if (!empty($data['authors'])) {
                    foreach ($data['authors'] as $author) {
                        $termId = wp_set_object_terms($postId, $author['name'], 'fandl_feed_authors', true);
                        if (is_array($termId)) {
                            $termId = reset($termId);
                        }

                        // Update the author
                        if ($author['job_title']) {
                            update_term_meta($termId, 'fandl_author_job_title', $author['job_title']);
                        }
                        if ($author['bio']) {
                            update_term_meta($termId, 'fandl_author_bio', $author['bio']);
                        }
                        if ($author['photo']) {
                            update_term_meta($termId, 'fandl_author_photo', $author['photo']);
                        }
                    } // foreach
                }

                // Force status to publish
                wp_publish_post($postId);

                return true;
            }
        }
        return false;
    }

    public function syncFeed() {
        global $wpdb;

        // Import all content feed
        if ($this->importFeed()) {
            // Tables
            $tablePosts = $wpdb->prefix . 'posts';
            $tablePostMeta = $wpdb->prefix . 'postmeta';
            $tableFeed = $this->getFeedTableName();

            // Count number of Published items in the feed table
            $feedCountResult = $wpdb->get_results("SELECT COUNT(*) AS total FROM {$tableFeed} WHERE status = '1'", ARRAY_A);
            $feedCountResult = reset($feedCountResult);
            $feedCount = isset($feedCountResult['total']) ? (int) $feedCountResult['total'] : 0;

            // Only attempts to unpublish or update posts if the feed table is not empty
            if ($feedCount) {
                // Posts to be unpublished
                $postsUnpublish = $wpdb->get_results("SELECT p.ID FROM {$tablePosts} p INNER JOIN {$tablePostMeta} pm ON p.ID = pm.post_id AND pm.meta_key = 'fandl_nid' INNER JOIN {$tableFeed} ff ON pm.meta_value = ff.nid WHERE p.post_type = 'fandl_feed_article' AND p.post_status IN ('publish', 'future', 'private') AND (ff.status IS NULL OR ff.status != '1')", OBJECT_K);
                $postsUnpublish = array_keys($postsUnpublish);
                foreach ($postsUnpublish as $postId) {
                    // Unpublish posts that are no longer active in the Content Feed
                    wp_update_post(array(
                        'ID' => $postId,
                        'post_status' => 'draft',
                        'meta_input' => array(
                            'fandl_unpublished' => date('c')
                        )
                    ));
                }

                // Posts to be updated
                $postsUpdate = $wpdb->get_results("SELECT pm.meta_value AS nid, pm2.meta_value AS post_changed, ff.changed AS feed_changed FROM {$tablePosts} p INNER JOIN {$tablePostMeta} pm ON p.ID = pm.post_id AND pm.meta_key = 'fandl_nid' INNER JOIN {$tablePostMeta} pm2 ON p.ID = pm2.post_id AND pm2.meta_key = 'fandl_changed' INNER JOIN {$tableFeed} ff ON pm.meta_value = ff.nid WHERE p.post_type = 'fandl_feed_article' AND p.post_status IN ('publish', 'future', 'private') AND ff.status = '1'", ARRAY_A);
                foreach ($postsUpdate as $postArray) {
                    $nid = (int) $postArray['nid'];
                    $feedChanged = strtotime($postArray['feed_changed']);
                    $postChanged = strtotime($postArray['post_changed']);
                    // If content needs updating
                    if ($feedChanged > $postChanged) {
                        $this->importNode($nid);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function getFeedTableName() {
        global $wpdb;
        return $wpdb->prefix . 'fandl_feed';
    }

}
