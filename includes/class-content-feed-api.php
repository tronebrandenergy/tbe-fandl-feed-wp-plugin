<?php
class ContentFeedApi {

    public function generateApiToken() {
        // Read in existing option value from database
        $apiHashSalt = get_option('fandl_feed_api_hash_salt');
        $apiHashToken = get_option('fandl_feed_api_hash_token');
        $apiHashAlgorithm = get_option('fandl_feed_api_hash_algorithm');

        // Generate and return the hash
        return hash_hmac($apiHashAlgorithm, $apiHashToken, date('Y-m-d') . $apiHashSalt);
    }

    public function requestApiList() {
        // Read in existing option value from database
        $apiBaseUrl = get_option('fandl_feed_api_base_url');

        // Pull paginated Feed from API
        $url = $apiBaseUrl . '/api/list';
        $page = 0;
        $maxIterations = 100;
        $results = [];
        do {
            $feedData = [];
            $responseData = $this->makeGetRequest($url . '?page=' . $page);
            if ($responseData['status']) {
                $feedData = json_decode($responseData['content'], true);
                if ($feedData) {
                    $results = array_merge($results, $feedData);
                }
            } else {
                echo '<textarea rows="5" style="width: 100%;">'; var_dump($responseData['content']); echo '</textarea>';
            }
            $page++;
            if ($page >= $maxIterations) {
                break;
            }
        } while (!empty($feedData));
        return $results;
    }

    public function requestApiNode($nid) {
        // Read in existing option value from database
        $apiBaseUrl = get_option('fandl_feed_api_base_url');

        // Pull information about the Node from API
        $url = $apiBaseUrl . '/api/node/' . $nid;
        $feedData = [];
        $responseData = $this->makeGetRequest($url);
        if ($responseData['status']) {
            $feedData = json_decode($responseData['content'], true);
        } else {
            echo '<textarea rows="5" style="width: 100%;">'; var_dump($responseData['content']); echo '</textarea>';
        }
        return $feedData;
    }

    public function makeGetRequest($url) {
        $return = array(
            'status' => false,
            'content' => null
        );

        // HTTP request arguments
        $args = array(
            'headers' => array(
                'X-Token' => $this->generateApiToken()
            ),
            'timeout' => 30,
            'sslverify' => false,
        );

        // HTTP GET request
        $response = wp_remote_get($url, $args);

        // Error
        if (is_wp_error($response)) {
            $return['content'] = $response->get_error_message();
            return $return;
        }

        // Get response status code and body
        $responseCode = wp_remote_retrieve_response_code($response);
        $responseBody = wp_remote_retrieve_body($response);

        // Error
        if ($responseCode != 200) {
            $return['content'] = 'Invalid request. Status: ' . $responseCode . '. Response: ' . $responseBody;
            return $return;
        }

        // Success
        $return['status'] = true;
        $return['content'] = $responseBody;
        return $return;
    }

}
