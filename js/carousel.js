(function($){
  //slick slider


  $('.slider-for').each(function(key, item) {

    var sliderIdName = 'slider' + key;
    var sliderNavIdName = 'slider-nav' + key;
    var sliderDotIdName = 'slider-dots' + key;

    this.id = sliderIdName;
    $('.slider-nav')[key].id = sliderNavIdName;
    $('.slider-dots')[key].id = sliderDotIdName;

    var sliderId = '#' + sliderIdName;
    var sliderNavId = '#' + sliderNavIdName;
    var sliderDotId = '#' + sliderDotIdName;

    $(sliderId).slick({
      fade: true,
      dots: true,
      appendDots: sliderDotId,
      accessibility: true,
      adaptiveHeight: true,
      asNavFor: sliderNavId
    });

    $(sliderNavId).slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: sliderId,
      accessibility: true,
      vertical:true,
      arrows: false,
      centerMode: false,
      focusOnSelect: true,
      verticalSwiping:true,
    });

  });


})(jQuery);
