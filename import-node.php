<?php
require('../../../wp-load.php');

// Load required classes
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/controller-content-feed.php');

$nid = isset($_REQUEST['nid']) ? (int) $_REQUEST['nid'] : 0;

// Import single node into the database
if ($nid) {
    $controller = new ContentFeedController();
    if ($controller->importNode($nid)) {
        wp_redirect(admin_url('edit.php?post_type=fandl_feed_article'));
        die;
    }

    wp_die('Error trying to import content.');
}

wp_die('Invalid request.');
