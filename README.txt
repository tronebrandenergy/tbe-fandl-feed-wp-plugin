=== Faith & Leadership - Content Feed ===
Author: Trone Brand Energy
Version: 1.0

== Description ==

Client plugin to import content from Faith & Leadership (https://www.faithandleadership.com/).

== Installation ==

1. Manually install the plugin by unzipping the folder in your website's 'plugins' folder.
2. Once activated, go to 'Content Feed > Settings' and enter information to configure the Content Feed API:
- API Base URL: https://www.faithandleadership.com
- API Hash Salt: check with Faith & Leadership
- API Hash Token: check with Faith & Leadership
- API Hash Algorithm: check with Faith & Leadership
3. Click on 'Save'.
4. Click on 'Import Feed' to pull a list with all the content available in Faith & Leadership.

== Usage ==

After initial feed import (by clicking on 'Import Feed' in the Settings page), all available content will be listed under 'Content Feed > Feed'.
The feed is a list with all available articles in Faith & Leadership, along with a 'Import' (or 'Update') button. By clicking on 'Import', the corresponding article is imported in its full format and saved as a post in your website. Imported articles can also be re-imported by clicking on 'Update', overriding all built-in fields with the most recent content from Faith & Leadership.

All imported content is listed under 'Content Feed > Articles'. Once an article has been imported into your website, it behaves like a regular post. Articles can be edited and customized freely, however, any built-in field in the article will get overwritten if the article is manually synchronized from Faith & Leadership (by clicking on 'Update' in the Feed list).

== Actions ==

In the Settings page, there are two buttons at the bottom: 'Import Feed' and 'Synchronize Feed'.
- Import Feed: This action should be used to pull the initial list of content after installation. By clicking on 'Import Feed', the current feed gets updated and a fresh list of content is pulled from Faith & Leadership. Please note that this only affects the feed, and not the articles that have been already imported to your website. No articles are deleted in this action.
- Synchronize Feed: This action imports the entire feed again (just like 'Import Feed'), and after that, unpublishes any previously imported articles that are no longer available in Faith & Leadership. This action doesn't delete any articles, just unpublishes content by changing article status to 'Draft'. This operation is also executed automatically once a day via WP-Cron.

== Articles ==

Articles imported from Faith & Leadership are saved in your website using the custom post type 'fandl_feed_article'. This custom post type behaves exactly like a regular 'post', with some additional custom fields:
- fandl_nid: Original article ID (Node ID) from Faith & Leadership.
- fandl_image_caption: Featured image caption.
- fandl_image_credit: Featured image credits.
- fandl_canonical_url: Original canonical URL for the article in Faith & Leadership. Useful for SEO.
- fandl_metatag_title: Meta tag title in Faith & Leadership.
- fandl_metatag_description: Meta description in Faith & Leadership.
- fandl_article_date: Article date as displayed in Faith & Leadership.
- fandl_changed: Date and time of the last update in Faith & Leadership.
- fandl_created: Date and time this article was originally authored in Faith & Leadership.

== Authors ==

The custom taxonomy for Authors (fandl_feed_authors) has three custom fields:
- fandl_author_job_title: Author's job title.
- fandl_author_bio: Editor field with the full author's bio.
- fandl_author_photo: URL to the author's photo.

== Special HTML markup examples ==

# Callouts
<div class="callout question default-visibility question">
    <div class="inner">
        <p>Who is working to “rehab hate” in your community? How is the church participating in this work?</p>
    </div>
</div>

# Questions to Consider
<div class="questions-box">
    <div class="left">
        <h3>Questions to consider</h3>
    </div>
    <div class="inner-content">
        <ul>
            <li>Who is working to “rehab hate” in your community? How is the church participating in this work?</li>
            <li>How is the Rev. David Kennedy’s dual formation through the frayed lynching rope and the lessons of Sunday school similar to or different from your own formation? How does this formation show up in your daily life?</li>
            <li>How does Kennedy’s commitment to embodying Jesus’ teaching in Matthew 25:34-40 call you and your community to an active faith?</li>
            <li>What spaces in your community are ripe for physical and spiritual rehabilitation?</li>
        </ul>
    </div>
</div>
