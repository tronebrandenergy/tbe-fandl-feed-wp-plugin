<?php
require('../../../wp-load.php');

// Load required classes
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/controller-content-feed.php');

// Resync all content feed
$controller = new ContentFeedController();
$controller->syncFeed();

wp_redirect(admin_url('admin.php?page=tbe_fandl_feed'));
die;
