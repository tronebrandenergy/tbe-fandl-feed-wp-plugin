<?php
require('../../../wp-load.php');

// Load required classes
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/controller-content-feed.php');

// Import all content feed
$controller = new ContentFeedController();
if ($controller->importFeed()) {
    wp_redirect(admin_url('admin.php?page=tbe_fandl_feed'));
    die;
}

wp_die('Error trying to import content.');
