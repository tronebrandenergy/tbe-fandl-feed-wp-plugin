<?php
/*
 * Plugin Name: Faith & Leadership - Content Feed
 */

define('TBE_FANDL_FEED_DB_VERSION', '1.1');
define('TBE_FANDL_FEED_ABSPATH', plugin_dir_path(__FILE__));

// Load required classes
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/class-content-feed-table.php');
require_once(TBE_FANDL_FEED_ABSPATH . 'includes/controller-content-feed.php');

// Admin menu
function tbe_fandl_feed_admin_menu() {
    // add_menu_page(string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '', string $icon_url = '', int $position = null)
    add_menu_page('Faith & Leadership', 'Content Feed', 'activate_plugins', 'tbe_fandl_feed', 'tbe_fandl_feed_render_list_page', 'dashicons-download', 10);

    // add_submenu_page(string $parent_slug, string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '', int $position = null)
    add_submenu_page('tbe_fandl_feed', 'Feed', 'Feed', 'activate_plugins', 'tbe_fandl_feed', 'tbe_fandl_feed_render_list_page', 10);
    add_submenu_page('tbe_fandl_feed', 'Articles', 'Articles', 'activate_plugins', 'edit.php?post_type=fandl_feed_article', '', 15);
    add_submenu_page('tbe_fandl_feed', 'Topics', 'Topics', 'activate_plugins', 'edit-tags.php?taxonomy=fandl_feed_topics', '', 20);
    add_submenu_page('tbe_fandl_feed', 'Authors', 'Authors', 'activate_plugins', 'edit-tags.php?taxonomy=fandl_feed_authors', '', 25);
    add_submenu_page('tbe_fandl_feed', 'Settings', 'Settings', 'activate_plugins', 'tbe_fandl_feed_settings', 'tbe_fandl_feed_render_settings_page', 30);
}
add_action('admin_menu', 'tbe_fandl_feed_admin_menu');

function tbe_fandl_feed_render_settings_page() {
    // must check that the user has the required capability
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    // Read in existing option value from database
    $apiBaseUrl = get_option('fandl_feed_api_base_url');
    $apiHashSalt = get_option('fandl_feed_api_hash_salt');
    $apiHashToken = get_option('fandl_feed_api_hash_token');
    $apiHashAlgorithm = get_option('fandl_feed_api_hash_algorithm');

    // See if the user has posted us some information
    if (isset($_POST['post']) && $_POST['post'] == 'Y') {
        // Read their posted value
        $apiBaseUrl = trim($_POST['fandl_feed_api_base_url']);
        $apiHashSalt = trim($_POST['fandl_feed_api_hash_salt']);
        $apiHashToken = trim($_POST['fandl_feed_api_hash_token']);
        $apiHashAlgorithm = trim($_POST['fandl_feed_api_hash_algorithm']);

        // Save the posted value in the database
        update_option('fandl_feed_api_base_url', $apiBaseUrl);
        update_option('fandl_feed_api_hash_salt', $apiHashSalt);
        update_option('fandl_feed_api_hash_token', $apiHashToken);
        update_option('fandl_feed_api_hash_algorithm', $apiHashAlgorithm);

        // Put a "settings saved" message on the screen
        ?>
        <div class="updated">
            <p><strong><?php _e('Settings saved.'); ?></strong></p>
        </div>
        <?php
    }

    // settings form
    ?>
    <div class="wrap">
        <h2>Faith & Leadership - Content Feed - Settings</h2>
        <form name="form-fandl-settings" method="post" action="">
            <table class="form-table" role="presentation">
                <tbody>
                    <tr>
                        <th scope="row"><label for="fandl_feed_api_base_url">API Base URL</label></th>
                        <td><input id="fandl_feed_api_base_url" name="fandl_feed_api_base_url" type="text" value="<?php echo htmlentities($apiBaseUrl, ENT_COMPAT, 'UTF-8'); ?>" class="regular-text" /></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="fandl_feed_api_hash_salt">API Hash Salt</label></th>
                        <td><input id="fandl_feed_api_hash_salt" name="fandl_feed_api_hash_salt" type="text" value="<?php echo htmlentities($apiHashSalt, ENT_COMPAT, 'UTF-8'); ?>" class="regular-text" /></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="fandl_feed_api_hash_token">API Hash Token</label></th>
                        <td><input id="fandl_feed_api_hash_token" name="fandl_feed_api_hash_token" type="text" value="<?php echo htmlentities($apiHashToken, ENT_COMPAT, 'UTF-8'); ?>" class="regular-text" /></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="fandl_feed_api_hash_algorithm">API Hash Algorithm</label></th>
                        <td>
                            <select id="fandl_feed_api_hash_algorithm" name="fandl_feed_api_hash_algorithm">
                                <option value="sha256" <?php echo (isset($apiHashAlgorithm) && ($apiHashAlgorithm === 'sha256')) ? 'selected="selected"' : ''; ?>>sha256 (recommended)</option>
                                <option value="sha384" <?php echo (isset($apiHashAlgorithm) && ($apiHashAlgorithm === 'sha384')) ? 'selected="selected"' : ''; ?>>sha384</option>
                                <option value="sha512" <?php echo (isset($apiHashAlgorithm) && ($apiHashAlgorithm === 'sha512')) ? 'selected="selected"' : ''; ?>>sha512</option>
                                <option value="haval160,4" <?php echo (isset($apiHashAlgorithm) && ($apiHashAlgorithm === 'haval160,4')) ? 'selected="selected"' : ''; ?>>haval160,4</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="post" value="Y" />
            <p class="submit">
                <input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save') ?>" />
            </p>
            <hr />
            <br />
            <a href="<?php echo plugin_dir_url(__FILE__); ?>import-feed.php" class="button-primary">Import Feed</a>
            <a href="<?php echo plugin_dir_url(__FILE__); ?>sync-feed.php" class="button-primary">Synchronize Feed</a>
        </form>
    </div>
    <?php
}

function tbe_fandl_feed_render_list_page() {
    ?>
    <div class="wrap">
        <h2>Faith & Leadership - Content Feed</h2>
        <form method="post">
            <input type="hidden" value="tbe_fandl_feed" />
            <?php
            $table = new ContentFeedTable();
            $table->prepare_items();
            $table->search_box('Search', 'search_id');
            $table->display();
            ?>
        </form>
    </div>
    <?php
}

function tbe_fandl_feed_admin_head() {
    $page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? trim($_REQUEST['page']) : '';
    if ($page != 'tbe_fandl_feed') {
        return;
    }
    ?>
    <style>
        .wp-list-table .column-nid { width: 5%; }
        .wp-list-table .column-title { width: 40%; }
        .wp-list-table .column-authors { width: 20%; }
        .wp-list-table .column-created { width: 15%; }
        .wp-list-table .column-status { width: 10%; }
        .wp-list-table .column-actions { width: 10%; }
    </style>
    <?php
}
add_action('admin_head', 'tbe_fandl_feed_admin_head');

function tbe_fandl_feed_sync() {
    // Resync all content feed
    $controller = new ContentFeedController();
    $controller->syncFeed();
}
add_action('tbe_fandl_feed_cron_hook', 'tbe_fandl_feed_sync');

// Install database tables upon plugin activation
function tbe_fandl_feed_install() {
    global $wpdb;

    $table_name = $wpdb->prefix . 'fandl_feed';
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id int(11) NOT NULL AUTO_INCREMENT,
        nid int(11) NOT NULL,
        title varchar(255) NOT NULL,
        authors varchar(255) NULL,
        status char(1) NULL,
        changed datetime DEFAULT '0000-00-00 00:00:00' NULL,
        created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY  (id),
        KEY idx_nid (nid),
        KEY idx_status (status)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

    $currentDbVersion = get_option('tbe_fandl_feed_db_version');
    if ($currentDbVersion) {
        update_option('tbe_fandl_feed_db_version', TBE_FANDL_FEED_DB_VERSION);
    } else {
        add_option('tbe_fandl_feed_db_version', TBE_FANDL_FEED_DB_VERSION);
    }

    // Setup cron job to sync the feed
    if (!wp_next_scheduled('tbe_fandl_feed_cron_hook')) {
        wp_schedule_event(time(), 'daily', 'tbe_fandl_feed_cron_hook');
    }
}
register_activation_hook(__FILE__, 'tbe_fandl_feed_install');

// Check if the database table needs to be updated
function tbe_fandl_feed_update_db_check() {
    if (get_option('tbe_fandl_feed_db_version') != TBE_FANDL_FEED_DB_VERSION) {
        tbe_fandl_feed_install();

        // Truncate table after upgrade
        $controller = new ContentFeedController();
        $controller->truncateFeed();
    }
}
add_action('plugins_loaded', 'tbe_fandl_feed_update_db_check');

function tbe_fandl_feed_init() {
    // Taxonomy - Topics
    $labels = array(
        'name' => __('Topics'),
        'singular_name' => __('Topic'),
        'search_items' => __('Search Topics'),
        'all_items' => __('All Topics'),
        'edit_item' => __('Edit Topic'),
        'update_item' => __('Update Topic'),
        'add_new_item' => __('Add New Topic'),
        'new_item_name' => __('New Topic'),
        'menu_name' => __('Topics')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'hierarchical' => true,
        'show_in_rest' => true,
        'show_in_menu' => true
    );
    register_taxonomy('fandl_feed_topics', array('fandl_feed_article'), $args);

    // Taxonomy - Authors
    $labels = array(
        'name' => __('Authors'),
        'singular_name' => __('Author'),
        'search_items' => __('Search Authors'),
        'all_items' => __('All Authors'),
        'edit_item' => __('Edit Author'),
        'update_item' => __('Update Author'),
        'add_new_item' => __('Add New Author'),
        'new_item_name' => __('New Author'),
        'menu_name' => __('Authors')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'hierarchical' => true,
        'show_in_rest' => true,
        'show_in_menu' => true
    );
    register_taxonomy('fandl_feed_authors', array('fandl_feed_article'), $args);

    // Post Type - Articles
    $labels = array(
        'name' => __('Articles'),
        'singular_name' => __('Article'),
        'add_new' => __('Add Article'),
        'add_new_item' => __('Add New Article'),
        'edit_item' => __('Edit Article'),
        'new_item' => __('New Article'),
        'all_items' => __('All Articles'),
        'view_item' => __('View Article'),
        'search_items' => __('Search Articles'),
        'not_found' => __('No Articles found'),
        'not_found_in_trash' => __('No Articles found in the Trash'),
        'menu_name' => 'Articles'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Holds the content imported from the Faith & Leadership Content Feed',
        'public' => true,
        'menu_position' => 10,
        'taxonomies' => array('fandl_feed_topics', 'fandl_feed_authors', 'post_tag'),
        'menu_icon' => 'dashicons-location-alt',
        'show_in_rest' => true,
        'supports' => array('title', 'editor', 'author', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields'),
        'has_archive' => true,
        'show_in_menu' => false
    );
    register_post_type('fandl_feed_article', $args);
}
add_action('init', 'tbe_fandl_feed_init');

// Function to fix the Admin menu
function tbe_fandl_feed_taxonomy_menu_open($parent_file) {
    global $current_screen;
    $taxonomy = $current_screen->taxonomy;
    if (in_array($taxonomy, array('fandl_feed_topics', 'fandl_feed_authors'))) {
        $parent_file = 'tbe_fandl_feed';
    }
    return $parent_file;
}
add_action('parent_file', 'tbe_fandl_feed_taxonomy_menu_open');

// Additional fields for the Authors taxonomy
function tbe_fandl_feed_authors_add_taxonomy_custom_fields($taxonomy) {
    ?>
    <div class="form-field">
        <label for="fandl_author_job_title">Job Title</label>
        <input id="fandl_author_job_title" type="text" name="fandl_author_job_title" />
    </div>
    <div class="form-field">
        <label for="fandl_author_bio">Bio</label>
        <?php
        wp_editor(
            '',
            'fandl_author_bio',
            array(
                'textarea_rows' => 5,
                'teeny' => true,
                'media_buttons' => false
            )
        );
        ?>
    </div>
    <div class="form-field">
        <label for="fandl_author_photo">Photo</label>
        <input id="fandl_author_photo" type="text" name="fandl_author_photo" />
    </div>
    <?php
}
add_action('fandl_feed_authors_add_form_fields', 'tbe_fandl_feed_authors_add_taxonomy_custom_fields', 10, 1);

// Additional fields for the Authors taxonomy
function tbe_fandl_feed_authors_edit_taxonomy_custom_fields($term, $taxonomy) {
    ?>
    <table class="form-table" role="presentation">
        <tbody>
            <tr class="form-field term-description-wrap">
                <th scope="row">
                    <label for="fandl_author_job_title">Job Title</label>
                </th>
                <td>
                    <input id="fandl_author_job_title" type="text" name="fandl_author_job_title" value="<?php echo esc_attr(get_term_meta($term->term_id, 'fandl_author_job_title', true)); ?>" />
                </td>
            </tr>
            <tr class="form-field term-description-wrap">
                <th scope="row">
                    <label for="fandl_author_bio">Bio</label>
                </th>
                <td>
                    <?php
                    wp_editor(
                        get_term_meta($term->term_id, 'fandl_author_bio', true),
                        'fandl_author_bio',
                        array(
                            'textarea_rows' => 5,
                            'teeny' => true,
                            'media_buttons' => false
                        )
                    );
                    ?>
                </td>
            </tr>
            <tr class="form-field term-description-wrap">
                <th scope="row">
                    <label for="fandl_author_photo">Photo</label>
                </th>
                <td>
                    <input id="fandl_author_photo" type="text" name="fandl_author_photo" value="<?php echo esc_attr(get_term_meta($term->term_id, 'fandl_author_photo', true)); ?>" />
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}
add_action('fandl_feed_authors_edit_form', 'tbe_fandl_feed_authors_edit_taxonomy_custom_fields', 10, 2);

// Save additional fields for the Authors taxonomy
function tbe_fandl_feed_authors_save_taxonomy_custom_fields($term_id) {
    $fields = [
        'fandl_author_job_title',
        'fandl_author_bio',
        'fandl_author_photo'
    ];
    foreach ($fields as $field) {
        if (array_key_exists($field, $_POST)) {
            $field_value = $_POST[$field];
            if (!in_array($field, array('fandl_author_bio'))) {
                $field_value = sanitize_text_field($field_value);
            }
            update_term_meta($term_id, $field, $field_value);
        }
    }
}
add_action('created_fandl_feed_authors', 'tbe_fandl_feed_authors_save_taxonomy_custom_fields');
add_action('edited_fandl_feed_authors', 'tbe_fandl_feed_authors_save_taxonomy_custom_fields');

// Plugin deactivation hook
function tbe_fandl_feed_deactivate() {
    global $wpdb;

    $timestamp = wp_next_scheduled('tbe_fandl_feed_cron_hook');
    wp_unschedule_event($timestamp, 'tbe_fandl_feed_cron_hook');

    // Drop table
    $controller = new ContentFeedController();
    $table = $controller->getFeedTableName();
    $dropQuery = 'DROP TABLE IF EXISTS ' . $table;
    $wpdb->query($dropQuery);
}
register_deactivation_hook(__FILE__, 'tbe_fandl_feed_deactivate');

/**
 * Register meta boxes.
 */
function tbe_fandl_feed_add_meta_boxes() {
    // add_meta_box( $id, $title, $callback, $screen, $context, $priority, $callback_args)
    add_meta_box('fandl-metabox-1', 'Additional fields', 'tbe_fandl_feed_meta_box_render', 'fandl_feed_article');
}
add_action('add_meta_boxes', 'tbe_fandl_feed_add_meta_boxes');

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function tbe_fandl_feed_meta_box_render($post) {
    ?>
    <div class="fandl_custom_box">
        <style scoped>
            .fandl_custom_box {
                display: grid;
                grid-template-columns: max-content 1fr;
                grid-row-gap: 10px;
                grid-column-gap: 20px;
            }
            .fandl_custom_field {
                display: contents;
            }
        </style>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_nid">Node ID</label>
            <input id="fandl_nid" type="text" name="fandl_nid" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_nid', true)); ?>" />
        </p>
        <div class="meta-options fandl_custom_field">
            <label for="fandl_additional_1">Additional Field 1</label>
            <?php
            wp_editor(
                get_post_meta(get_the_ID(), 'fandl_additional_1', true),
                'fandl_additional_1',
                array(
                    'textarea_rows' => 5,
                    'teeny' => true
                )
            );
            ?>
        </div>
        <div class="meta-options fandl_custom_field">
            <label for="fandl_image_caption">Image Caption</label>
            <?php
            wp_editor(
                get_post_meta(get_the_ID(), 'fandl_image_caption', true),
                'fandl_image_caption',
                array(
                    'textarea_rows' => 5,
                    'teeny' => true
                )
            );
            ?>
        </div>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_image_credit">Image Credit</label>
            <input id="fandl_image_credit" type="text" name="fandl_image_credit" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_image_credit', true)); ?>" />
        </p>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_canonical_url">Canonical URL</label>
            <input id="fandl_canonical_url" type="text" name="fandl_canonical_url" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_canonical_url', true)); ?>" />
        </p>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_metatag_title">Metatag Title</label>
            <input id="fandl_metatag_title" type="text" name="fandl_metatag_title" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_metatag_title', true)); ?>" />
        </p>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_metatag_description">Metatag Description</label>
            <textarea id="fandl_metatag_description" rows="3" name="fandl_metatag_description"><?php echo esc_textarea(get_post_meta(get_the_ID(), 'fandl_metatag_description', true)); ?></textarea>
        </p>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_article_date">Article Date</label>
            <input id="fandl_article_date" type="text" name="fandl_article_date" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_article_date', true)); ?>" />
        </p>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_changed">Changed</label>
            <input id="fandl_changed" type="text" name="fandl_changed" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_changed', true)); ?>" />
        </p>
        <p class="meta-options fandl_custom_field">
            <label for="fandl_created">Created</label>
            <input id="fandl_created" type="text" name="fandl_created" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'fandl_created', true)); ?>" />
        </p>
    </div>
    <?php
}

/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function tbe_fandl_feed_meta_box_save($post_id) {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if ($parent_id = wp_is_post_revision($post_id)) {
        $post_id = $parent_id;
    }

    $fields = [
        'fandl_nid',
        'fandl_additional_1',
        'fandl_image_caption',
        'fandl_image_credit',
        'fandl_canonical_url',
        'fandl_metatag_title',
        'fandl_metatag_description',
        'fandl_article_date',
        'fandl_changed',
        'fandl_created',
    ];
    foreach ($fields as $field) {
        if (array_key_exists($field, $_POST)) {
            $field_value = $_POST[$field];
            if (!in_array($field, array('fandl_additional_1', 'fandl_image_caption'))) {
                $field_value = sanitize_text_field($field_value);
            }
            update_post_meta($post_id, $field, $field_value);
        }
    }
}
add_action('save_post', 'tbe_fandl_feed_meta_box_save');

function tbe_fandl_feed_style() {
    wp_register_style('tbe_fandl_feed_custom', plugin_dir_url(__FILE__) . 'css/custom.css', array(), '1.0', 'all');
    wp_enqueue_style('tbe_fandl_feed_custom');

    // Slick Carousel
    wp_enqueue_style('slick', plugin_dir_url(__FILE__) . 'js/slick/slick.css', array(), '1.8.0');
    wp_enqueue_style('slick-theme', plugin_dir_url(__FILE__) . 'js/slick/slick-theme.css', array(), '1.8.0');
    wp_enqueue_script('slick', plugin_dir_url(__FILE__) . 'js/slick/slick.min.js', array(), '1.8.0', true);
    wp_enqueue_script('carousel', plugin_dir_url(__FILE__) . 'js/carousel.js', array('jquery'), '1.8.0', true);
}
add_action('wp_enqueue_scripts', 'tbe_fandl_feed_style');
