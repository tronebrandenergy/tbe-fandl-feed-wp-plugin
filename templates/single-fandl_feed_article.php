<?php
/**
 * Sample template for Faith & Leadership articles based on the theme 'Twenty Twenty-One'
 */

get_header();

while (have_posts()) {
    the_post();
    ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header alignwide">
            <?php
            // Additional Field 1
            $fandlAdditional1 = get_post_meta(get_the_ID(), 'fandl_additional_1', true);
            if ($fandlAdditional1) {
                ?>
                <div class="entry-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell medium-offset-1 medium-7 top-align">
                            <?php echo $fandlAdditional1; ?>
                        </div>
                    </div>
                </div>
                <?php
            }

            the_title('<h1 class="entry-title">', '</h1>');

            // Excerpt
            if (has_excerpt()) {
                ?>
                <div class="entry-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell medium-offset-1 medium-7 top-align">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <figure class="post-thumbnail">
                <?php
                // Lazy-loading attributes should be skipped for thumbnails since they are immediately in the viewport.
                the_post_thumbnail('post-thumbnail', array('loading' => false));

                // Caption and credits
                $fandlImageCaption = get_post_meta(get_the_ID(), 'fandl_image_caption', true);
                $fandlImageCredit = get_post_meta(get_the_ID(), 'fandl_image_credit', true);
                if ($fandlImageCaption) {
                    ?>
                    <figcaption class="wp-caption-text"><?php echo $fandlImageCaption; ?></figcaption>
                    <?php
                } else if (wp_get_attachment_caption(get_post_thumbnail_id())) {
                    ?>
                    <figcaption class="wp-caption-text"><?php echo wp_kses_post(wp_get_attachment_caption(get_post_thumbnail_id())); ?></figcaption>
                    <?php
                }
                if ($fandlImageCredit) {
                    ?>
                    <figcaption class="wp-caption-text"><?php echo $fandlImageCredit; ?></figcaption>
                    <?php
                }
                ?>
            </figure>
        </header>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <footer class="entry-footer default-max-width">
            <div class="posted-by">
                <?php
                // Created
                $fandlCreated = get_post_meta(get_the_ID(), 'fandl_created', true);
                ?>
                <span class="posted-on">
                    <time class="entry-date published updated"><?php printf(esc_html__('Published %s', 'twentytwentyone'), date('l, F d Y', strtotime($fandlCreated))); ?></time>
                </span>
                <span class="byline"><?php echo get_the_term_list(get_the_ID(), 'fandl_feed_authors', 'By ', ', ', ''); ?></span>
            </div>
            <div class="post-taxonomies">
                <span class="cat-links"><?php echo get_the_term_list(get_the_ID(), 'fandl_feed_topics', 'Topics: ', ', ', ''); ?></span>
            </div>
        </footer>
    </article>
    <?php
} // while

get_footer();
